<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{page?}', 'IndexController@index')
          ->where('page', '[0-9]+');

Route::post('/search/{page?}', 'IndexController@search')
          ->where('page', '[0-9]+');

Route::get('/api/{id}', 'ApiController@index')
          ->where('id', '[0-9]+');

Route::fallback(function () {
    //404
    return view('404');
});
