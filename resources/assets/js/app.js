
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.pokemon-link').on('click',function(){

        $('#loading-overlay').addClass('visible');

        var pokeId = $(this).data('pokeid') + 1;

        $.ajax({
          url: "/api/"+pokeId,
          context: document.body
        }).done(function(data) {

          var pokemon = JSON.parse(data);
          $('#pokmemonTitle').html(pokemon.name);
          $('#pokmemonSpecies').html(pokemon.species);
          $('#pokmemonHeight').html(pokemon.height);
          $('#pokmemonWeight').html(pokemon.weight);
          $('#pokemonImg').css('background-image','url('+pokemon.img+')');

          $.each(pokemon.abilities, function( index, value ) {
            $('#pokmemonAbilities').append('<li>'+value.ability.name+'</li>');
          });

          $('#loading-overlay').removeClass('visible');
          $('#detailModal').modal('show');
        });
    });
});

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
