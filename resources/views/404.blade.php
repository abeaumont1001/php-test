@extends('layout')

@section('content')
  <div class="container">
      <h1>404 not found</h1>

      <a class="btn btn-outline-info btn-lg pokemon-link" href="{{ url('/') }}">Home</a>

  </div>
@endsection
