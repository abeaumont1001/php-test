@extends('layout')

@section('content')
  <div class="container">
      <h1>Pokedex</h1>

      <div class="clearfix">
            <form method="POST" action="/search">
                @csrf
                <div class="input-group mb-3">
                  <input name="term" value="@if(isset($term)){{$term}}@endif" type="text" class="form-control" placeholder="search Name" aria-label="search Name" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit">GO</button>
                  </div>
                </div>
            </form>

            @if(isset($term))
            <a href="{{url('/')}}" class="btn btn-outline-secondary clear-button" >
              Clear Search
            </a>
            @endif
      </div>

      @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      <h4>Total Results({{$total}}):</h4>

      @foreach($all as $key => $pokemon)
        <a class="btn btn-outline-info btn-lg pokemon-link" href="#" data-pokeid="{{$pokemon['id']-1}}">{{$pokemon['name']}}</a>
      @endforeach

      <nav class="mt-3">
        <ul class="pagination">
          @for ($i = 1; $i < $pages; $i++)
              <li class="page-item {{ $page == $i ? 'active' : '' }}"><a class="page-link" href="/{{$i}}">{{$i}}</a></li>
          @endfor
        </ul>
      </nav>

  </div>

  <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title" id="pokmemonTitle"></h2>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="pokemonImg"></div>
          <p><strong>Species:</strong> <span id="pokmemonSpecies"></span></p>
          <p><strong>Height:</strong> <span id="pokmemonHeight"></span></p>
          <p><strong>Weight:</strong> <span id="pokmemonWeight"></span></p>
          <p><strong>Abilities:</strong></p>
          <ul id="pokmemonAbilities"></ul>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

@endsection
