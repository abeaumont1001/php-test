<!doctype html>
<html lang="en">

  @include('partial._head')

  <body>

    @yield('content')

    <div id="loading-overlay">
      <h3>Loading Pokemon...</h3>
    </div>
    
    @include('partial._foot')

  </body>
</html>
