<?php namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use PokePHP\PokeApi;

class PokeapiRepository implements PokeapiRepositoryInterface
{
    protected $api;
    protected $limit;

    // Constructor to bind model to repo
    public function __construct(PokeApi $pokeApi)
    {
        $this->api = $pokeApi;
        $this->limit = 20;
    }

    public function _get($key)
    {
      return $this->{$key};
    }

    public function all($page)
    {
      $pokemon = json_decode($this->api->resourceList('pokemon', $this->limit, ($page-1) * $this->limit));

      //cache number of pokemon
      Cache::put('pokemon-count', $pokemon->count, 1440);

      return $this->collectPokemon($pokemon->results);
    }

    public function allNames()
    {
      //retrieve from cache if available
      $names = Cache::remember('pokemon-names', 1440 , function () {

        //get number of pokemon from cache
        $count = Cache::get('pokemon-count');

        //get list of names from api
        $pokemon = json_decode($this->api->resourceList('pokemon', $count))->results;

        //convert to simple array of names
        $collection = $this->collectPokemon($pokemon);

        return json_encode($collection);
      });

      return json_decode($names);
    }

    public function collectPokemon($pokemon)
    {
      //convert to simple array of names
      return collect($pokemon)->map(function ($item, $key) {
          return ['name'=>$item->name,'id'=> explode('/',$item->url)[6]];
      });
    }

    public function getById($id)
    {
        return $this->api->pokemon($id);
    }

}
