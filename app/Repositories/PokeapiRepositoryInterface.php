<?php namespace App\Repositories;

interface PokeapiRepositoryInterface
{
    public function _get($key);
    public function all($page);
    public function allNames();
    public function getById($id);
    public function collectPokemon($pokemon);
}
