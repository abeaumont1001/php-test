<?php
namespace App\Http\Controllers;

use App\Repositories\PokeapiRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Show the main page.
     *
     * @param  int  $page
     * @param  PokeapiRepository $pokeaipRepository
     * @return Response
     */
    public function index($page = 1, PokeapiRepository $pokeaipRepository)
    {
        $all = $pokeaipRepository->all($page);
        $total = \Cache::get('pokemon-count');
        $pages = ($total / $pokeaipRepository->_get('limit') + 1);

        return view('index', compact('all','page','pages','total'));
    }

    /**
     * Show the search results.
     *
     * @param  int  $page
     * @param  PokeapiRepository $pokeaipRepository
     * @param  Request $request
     * @return Response
     */
    public function search($page = 1, PokeapiRepository $pokeaipRepository, Request $request)
    {
        //validate POST input
        $validated = $request->validate([
            'term' => 'required|max:100',
        ]);

        $term = strtolower($request->get('term'));

        //get list of names
        $all = collect($pokeaipRepository->allNames());
        //iterate through
        $all->each(function ($item, $key) use ($term, $all) {
          //check for partial string match with search term and pokemon name
          if (strpos($item->name, $term) === FALSE){
              //remove names that don't match
              $all->forget($key);
          }
        });

        $total = $all->count();

        $pages = ($total / $pokeaipRepository->_get('limit') + 1);

        return view('search', compact('all','page','pages','term','total'));
    }
}
