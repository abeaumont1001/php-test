<?php
namespace App\Http\Controllers;

use App\Repositories\PokeapiRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * return pokemon data.
     *
     * @param  int  $id
     * @param  PokeapiRepository $pokeaipRepository
     * @return Response
     */
    public function index($id = 1, PokeapiRepository $pokeaipRepository)
    {
      $pokemon = json_decode($pokeaipRepository->getById($id));

      return json_encode([
        'name' => $pokemon->name,
        'species' => $pokemon->species->name,
        'height' => $pokemon->height,
        'weight' => $pokemon->weight,
        'abilities' => $pokemon->abilities,
        'img' => $pokemon->sprites->front_default
      ]);
    }

}
